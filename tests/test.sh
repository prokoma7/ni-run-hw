#!/bin/bash
find -L . -type f -iname "*.fml" | while read file; do
	echo $file
	../fml run "$file"
	echo
done

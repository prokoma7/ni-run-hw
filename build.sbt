
name := "fml"

version := "0.1"

scalaVersion := "2.13.8"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-opt:inline")

libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.1"

// --------------------------------------------------------------------
// ASSEMBLY
// --------------------------------------------------------------------
assembly / assemblyJarName := "fml.jar"

assembly / mainClass := Some("fml.Main")

assemblyMergeStrategy in assembly := {
  case "META-INF/versions/11/module-info.class" => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
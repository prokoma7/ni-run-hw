package fml

import java.nio.ByteBuffer

package object bc {
  type ProgramLoc = Int
  type ConstantPool = IndexedSeq[ProgramObject]

  implicit class ByteBufferOps(that: ByteBuffer) {
    def getUnsignedInt: Long = that.getInt & 0xffffffff
    def getUnsignedInt(index: Int): Long = that.getInt(index) & 0xffffffff

    def getUnsignedShort: Int = that.getShort & 0xffff
    def getUnsignedShort(index: Int): Int = that.getShort(index) & 0xffff

    def getUnsigned: Int = that.get & 0xff
    def getUnsigned(index: Int): Int = that.get(index) & 0xff

    def putUnsignedInt(value: Long): Unit = that.putInt((value & 0xffffffff).toInt)
    def putUnsignedInt(index: Int, value: Long): Unit = that.putInt(index, (value & 0xffffffff).toInt)

    def putUnsignedShort(value: Int): Unit = that.putShort((value & 0xffff).toShort)
    def putUnsignedShort(index: Int, value: Int): Unit = that.putShort(index, (value & 0xffff).toShort)

    def putUnsigned(value: Int): Unit = that.put((value & 0xff).toByte)
    def putUnsigned(index: Int, value: Int): Unit = that.put(index, (value & 0xff).toByte)
  }

  implicit class ProgramLocOps(that: ProgramLoc) {
    def opcode(implicit program: Program): Int = program.getOpcode(that)
  }
}

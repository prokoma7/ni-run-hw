package fml.bc

import java.nio.ByteBuffer

case class SerializedInsn(length: Int, write: ByteBuffer => Unit) extends ProgramSerializable {
  override def writeTo(code: ByteBuffer): Unit = write(code)
}

sealed trait Insn {
  def opcode: Int

  def length: Int
}

case object Insn {
  def unapply(opcode: Int): Option[Insn] = opcode match {
    case OpLiteral.opcode => Some(OpLiteral)
    case OpGetLocal.opcode => Some(OpGetLocal)
    case OpSetLocal.opcode => Some(OpSetLocal)
    case OpGetGlobal.opcode => Some(OpGetGlobal)
    case OpSetGlobal.opcode => Some(OpSetGlobal)
    case OpCallFunction.opcode => Some(OpCallFunction)
    case OpReturn.opcode => Some(OpReturn)
    case OpLabel.opcode => Some(OpLabel)
    case OpJump.opcode => Some(OpJump)
    case OpBranch.opcode => Some(OpBranch)
    case OpPrint.opcode => Some(OpPrint)
    case OpArray.opcode => Some(OpArray)
    case OpObject.opcode => Some(OpObject)
    case OpGetField.opcode => Some(OpGetField)
    case OpSetField.opcode => Some(OpSetField)
    case OpCallMethod.opcode => Some(OpCallMethod)
    case OpDrop.opcode => Some(OpDrop)

    case _ => None
  }
}

sealed trait Insn3ConstantRef[T <: ProgramObject] extends Insn {
  val length = 3

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[ConstantRef[T]] =
    InsnHelper.matchOpcode(program, loc, opcode, InsnHelper.getConstantRef[T](program, loc + 1))

  def apply(ref: ConstantRef[T]): SerializedInsn =
    SerializedInsn(length, code => {
      code.putUnsigned(opcode)
      InsnHelper.putConstantRef(code, ref)
    })
}

sealed trait Insn1 extends Insn {
  val length = 1

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[Unit] =
    InsnHelper.matchOpcode(program, loc, opcode, ())

  def apply(): SerializedInsn = SerializedInsn(length, code => code.putUnsigned(opcode))
}

case object OpLiteral extends Insn3ConstantRef[ConstantLiteral] {
  val opcode = 0x01
}

case object OpGetLocal extends Insn {
  val opcode = 0x0a
  val length = 3

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[Int] =
    InsnHelper.matchOpcode(program, loc, opcode, program.code.getUnsignedShort(loc + 1))

  def apply(local: Int): SerializedInsn =
    SerializedInsn(length, code => {
      code.putUnsigned(opcode)
      code.putUnsignedShort(local)
    })
}

case object OpSetLocal extends Insn {
  val opcode = 0x09
  val length = 3

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[Int] =
    InsnHelper.matchOpcode(program, loc, opcode, program.code.getUnsignedShort(loc + 1))

  def apply(local: Int): SerializedInsn = SerializedInsn(length, code => {
    code.putUnsigned(opcode)
    code.putUnsignedShort(local)
  })
}

case object OpGetGlobal extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x0c
}

case object OpSetGlobal extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x0b
}

case object OpCallFunction extends Insn {
  val opcode = 0x08
  val length = 4

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[(ConstantRef[ConstantString], Int)] =
    InsnHelper.matchOpcode(program, loc, opcode, (InsnHelper.getConstantRef[ConstantString](program, loc + 1), program.code.getUnsigned(loc + 3)))

  def apply(nameRef: ConstantRef[ConstantString], arguments: Int): SerializedInsn = SerializedInsn(length, code => {
    code.putUnsigned(opcode)
    InsnHelper.putConstantRef(code, nameRef)
    code.putUnsigned(arguments)
  })
}

case object OpReturn extends Insn1 {
  val opcode = 0x0f
}

case object OpLabel extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x00
}

case object OpJump extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x0e
}

case object OpBranch extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x0d
}

case object OpPrint extends Insn {
  val opcode = 0x02
  val length = 4

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[(ConstantRef[ConstantString], Int)] =
    InsnHelper.matchOpcode(program, loc, opcode, (InsnHelper.getConstantRef[ConstantString](program, loc + 1), program.code.getUnsigned(loc + 3)))

  def apply(formatRef: ConstantRef[ConstantString], arguments: Int): SerializedInsn = SerializedInsn(length, code => {
    code.putUnsigned(opcode)
    InsnHelper.putConstantRef(code, formatRef)
    code.putUnsigned(arguments)
  })
}

case object OpArray extends Insn1 {
  val opcode = 0x03
}

case object OpObject extends Insn3ConstantRef[ConstantClass] {
  val opcode = 0x04
}

case object OpGetField extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x05
}

case object OpSetField extends Insn3ConstantRef[ConstantString] {
  val opcode = 0x06
}

case object OpCallMethod extends Insn {
  val opcode = 0x07
  val length = 4

  def unapply(loc: ProgramLoc)(implicit program: Program): Option[(ConstantRef[ConstantString], Int)] =
    InsnHelper.matchOpcode(program, loc, opcode, (InsnHelper.getConstantRef[ConstantString](program, loc + 1), program.code.getUnsigned(loc + 3)))

  def apply(nameRef: ConstantRef[ConstantString], arguments: Int): SerializedInsn = SerializedInsn(length, code => {
    code.putUnsigned(opcode)
    InsnHelper.putConstantRef(code, nameRef)
    code.putUnsigned(arguments)
  })
}

case object OpDrop extends Insn1 {
  val opcode = 0x10
}

object InsnIterator {
  def apply(program: Program, loc: ProgramLoc): Iterator[(ProgramLoc, Insn)] = apply(program.code, loc)

  def apply(code: ByteBuffer, loc: ProgramLoc): Iterator[(ProgramLoc, Insn)] =
    Iterator.unfold(loc)(loc => code.getUnsigned(loc) match {
      case Insn(op) => Some((loc, op), loc + op.length)
      case _ => None
    })
}

private object InsnHelper {
  @inline
  def matchOpcode[T](program: Program, loc: ProgramLoc, opcode: Int, fn: => T): Option[T] =
    if (program.getOpcode(loc) == opcode) Some(fn) else None

  @inline
  def getConstantRef[T <: ProgramObject](program: Program, loc: ProgramLoc): ConstantRef[T] = ConstantRef[T](program.code.getUnsignedShort(loc))

  @inline
  def putConstantRef[T <: ProgramObject](code: ByteBuffer, ref: ConstantRef[T]): Unit = code.putUnsignedShort(ref.index)
}
package fml.bc

import java.io.{InputStream, OutputStream}
import java.nio.channels.Channels
import java.nio.charset.StandardCharsets
import java.nio.{ByteBuffer, ByteOrder}
import java.util.concurrent.atomic.AtomicInteger

import scala.collection.mutable
import scala.util.Using

object ProgramReader {
  private def readTypeId(buf: ByteBuffer): Int = buf.getUnsigned

  private def readConstantRef[T <: ProgramObject](buf: ByteBuffer): ConstantRef[T] = ConstantRef[T](buf.getUnsignedShort)

  private def readProgramObject(buf: ByteBuffer): ProgramObject = readTypeId(buf) match {
    case ConstantInt.tag => ConstantInt(buf.getInt)

    case ConstantBoolean.tag => ConstantBoolean(buf.get == 0x01)

    case ConstantNull.tag => ConstantNull()

    case ConstantString.tag =>
      val lengthBytes = buf.getUnsignedInt.toInt
      val res = ConstantString(StandardCharsets.UTF_8.decode(buf.slice().limit(lengthBytes)).toString)
      buf.position(buf.position() + lengthBytes)
      res

    case ConstantSlot.tag => ConstantSlot(readConstantRef[ConstantString](buf))

    case ConstantMethod.tag =>
      val nameRef = readConstantRef[ConstantString](buf)
      val arguments = buf.getUnsigned
      val locals = buf.getUnsignedShort
      val lengthInsn = buf.getUnsignedInt.toInt

      val codeStartLoc = buf.position()
      val lengthBytes = InsnIterator(buf, codeStartLoc).take(lengthInsn).map(_._2.length).sum
      buf.position(buf.position() + lengthBytes)

      ConstantMethod(nameRef, arguments, locals, lengthInsn, lengthBytes, codeStartLoc)

    case ConstantClass.tag =>
      ConstantClass(0.until(buf.getUnsignedShort).map(_ => readConstantRef[ConstantMember](buf)))
  }

  private def readConstantPool(buf: ByteBuffer): ConstantPool = {
    0.until(buf.getUnsignedShort).map(_ => readProgramObject(buf))
  }

  private def readGlobalRefs(buf: ByteBuffer): IndexedSeq[ConstantRef[ConstantMember]] = {
    0.until(buf.getUnsignedShort).map(_ => readConstantRef[ConstantMember](buf))
  }

  def read(buf: ByteBuffer): Program = {
    buf.order(ByteOrder.LITTLE_ENDIAN)
    Program(buf, readConstantPool(buf), readGlobalRefs(buf), readConstantRef[ConstantMethod](buf))
  }

  def read(stream: InputStream): Program = read(ByteBuffer.wrap(stream.readAllBytes()))
}

trait ProgramSerializable {
  def length: Int

  def writeTo(code: ByteBuffer): Unit

  def writeTo(stream: OutputStream): Unit = {
    Using(Channels.newChannel(stream)) { channel =>
      channel.write(toByteBuffer)
    }.get
  }

  def toByteBuffer: ByteBuffer = {
    val buf = ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
    writeTo(buf)
    buf.rewind()
  }
}

class ConstantMethodBuilder(val nameRef: ConstantRef[ConstantString], val arguments: Int) extends ProgramSerializable {
  private var locals = 0
  private var nextLocalIndex = arguments
  private val codeInsns = mutable.Buffer.empty[SerializedInsn]

  def append(insnWriter: SerializedInsn): Unit =
    codeInsns.append(insnWriter)

  def appendLocal(): Int = {
    locals += 1
    nextLocalIndex += 1
    nextLocalIndex - 1
  }

  override def length: ProgramLoc =
    1 + 2 + 1 + 2 + 4 + codeInsns.map(_.length).sum

  override def writeTo(code: ByteBuffer): Unit = {
    code.putUnsigned(ConstantMethod.tag)
    code.putUnsignedShort(nameRef.index)
    code.putUnsigned(arguments)
    code.putUnsignedShort(locals)
    code.putUnsignedInt(codeInsns.size)
    codeInsns.foreach(_.writeTo(code))
  }
}

class ProgramBuilder extends ProgramSerializable {

  implicit class ProgramObjectSerializer(obj: ProgramObject) extends ProgramSerializable {
    override def length: ProgramLoc = obj match {
      case ConstantInt(_) => 1 + 4
      case ConstantBoolean(_) => 1 + 1
      case ConstantNull() => 1
      case ConstantString(value) => 1 + 4 + value.getBytes(StandardCharsets.UTF_8).length
      case ConstantSlot(_) => 1 + 2
      case ConstantClass(memberRefs) => 1 + 2 + 2 * memberRefs.size

      case _: ConstantMethod => throw new IllegalArgumentException("Use ConstantMethodBuilder instead.")
    }

    override def writeTo(code: ByteBuffer): Unit = obj match {
      case ConstantInt(value) =>
        code.putUnsigned(ConstantInt.tag)
        code.putInt(value)

      case ConstantBoolean(value) =>
        code.putUnsigned(ConstantBoolean.tag)
        code.putUnsigned(if (value) 1 else 0)

      case ConstantNull() => code.putUnsigned(ConstantNull.tag)

      case ConstantString(value) =>
        code.putUnsigned(ConstantString.tag)
        val buf = value.getBytes(StandardCharsets.UTF_8)
        code.putUnsignedInt(buf.length)
        code.put(buf)

      case ConstantSlot(nameRef) =>
        code.putUnsigned(ConstantSlot.tag)
        code.putUnsignedShort(nameRef.index.toShort)

      case ConstantClass(memberRefs) =>
        code.putUnsigned(ConstantClass.tag)
        code.putUnsignedShort(memberRefs.size)
        memberRefs.foreach(ref => code.putShort(ref.index.toShort))

      case _: ConstantMethod => throw new IllegalArgumentException("Use ConstantMethodBuilder instead.")
    }
  }

  private val constantPool = mutable.Buffer.empty[ProgramSerializable]
  private val globalRefs = mutable.Buffer.empty[ConstantRef[ConstantMember]]
  private var entryRef: Option[ConstantRef[ConstantMethod]] = None

  def append[T <: ProgramObject](obj: T): ConstantRef[T] = {
    constantPool.append(obj)
    ConstantRef[T](constantPool.size - 1)
  }

  def append(builder: ConstantMethodBuilder): ConstantRef[ConstantMethod] = {
    constantPool.append(builder)
    ConstantRef[ConstantMethod](constantPool.size - 1)
  }

  def appendGlobalRef(ref: ConstantRef[ConstantMember]): Unit =
    globalRefs.append(ref)

  def setEntryRef(ref: ConstantRef[ConstantMethod]): Unit =
    entryRef = Some(ref)

  override def length: ProgramLoc =
    2 + constantPool.map(_.length).sum + 2 + globalRefs.size * 2 + 2

  override def writeTo(code: ByteBuffer): Unit = {
    code.putUnsignedShort(constantPool.size.toShort)
    constantPool.foreach(_.writeTo(code))
    code.putUnsignedShort(globalRefs.size.toShort)
    globalRefs.foreach(ref => code.putUnsignedShort(ref.index))
    code.putUnsignedShort(entryRef.get.index)
  }

  def build(): Program = {
    val buf = ByteBuffer.allocate(length)
    writeTo(buf)
    ProgramReader.read(buf.rewind())
  }

  /** Convenience methods */

  private val labelCnt: AtomicInteger = new AtomicInteger(0)

  def appendNull(): ConstantRef[ConstantNull] = append(ConstantNull())

  def appendString(value: String): ConstantRef[ConstantString] = append(ConstantString(value))

  def appendBoolean(value: Boolean): ConstantRef[ConstantBoolean] = append(ConstantBoolean(value))

  def appendInt(value: Int): ConstantRef[ConstantInt] = append(ConstantInt(value))

  def appendSlot(name: String): ConstantRef[ConstantSlot] = append(ConstantSlot(appendString(name)))

  def appendLabelName(prefix: String): ConstantRef[ConstantString] = appendString(prefix + labelCnt.incrementAndGet().toString)

  def appendGlobalSlot(name: String): Unit = appendGlobalRef(appendSlot(name))

  def newMethodBuilder(name: String, arguments: Int) = new ConstantMethodBuilder(appendString(name), arguments)
}

/** Mixin to deduplicate multiple instances of an equal program object in the constant pool. */
trait ConstantPoolDeduplication extends ProgramBuilder {
  private val programObjectCache = mutable.Map.empty[ProgramObject, ConstantRef[ProgramObject]]

  override def append[T <: ProgramObject](obj: T): ConstantRef[T] =
    programObjectCache.getOrElseUpdate(obj, {
      super.append(obj)
    }).asInstanceOf[ConstantRef[T]]
}
package fml.bc

import java.nio.{ByteBuffer, ByteOrder}

import scala.collection.mutable
import scala.reflect.ClassTag

sealed trait ProgramObject

sealed trait ConstantLiteral extends ProgramObject

case class ConstantInt(value: Int) extends ConstantLiteral

object ConstantInt {
  val tag: Int = 0x00
}

case class ConstantBoolean(value: Boolean) extends ConstantLiteral

object ConstantBoolean {
  val tag: Int = 0x06
}

case class ConstantNull() extends ConstantLiteral

object ConstantNull {
  val tag: Int = 0x01
}

case class ConstantString(value: String) extends ProgramObject

object ConstantString {
  val tag: Int = 0x02
}

sealed trait ConstantMember extends ProgramObject {
  def nameRef: ConstantRef[ConstantString]
}

case class ConstantSlot(nameRef: ConstantRef[ConstantString]) extends ConstantMember

object ConstantSlot {
  val tag: Int = 0x04
}

case class ConstantMethod(nameRef: ConstantRef[ConstantString], arguments: Int, locals: Int, lengthInsn: Int, lengthBytes: Int, codeStartLoc: ProgramLoc) extends ConstantMember {
  def codeEndLoc: ProgramLoc = codeStartLoc + lengthBytes

  def codeIterator(implicit program: Program): Iterator[(ProgramLoc, Insn)] =
    InsnIterator(program, codeStartLoc).take(lengthInsn)

  def code(implicit program: Program): Iterable[(ProgramLoc, Insn)] = codeIterator.toSeq
}

object ConstantMethod {
  val tag: Int = 0x03
}

case class ConstantClass(memberRefs: IndexedSeq[ConstantRef[ConstantMember]]) extends ProgramObject {
  def fields(implicit constantPool: ConstantPool): Seq[ConstantSlot] =
    memberRefs collect { case ResolveConstantRef(s: ConstantSlot) => s }

  def methods(implicit constantPool: ConstantPool): Seq[ConstantMethod] =
    memberRefs collect { case ResolveConstantRef(m: ConstantMethod) => m }

  private var cacheBuilt: Boolean = false
  private val methodCache: mutable.Map[String, ConstantMethod] = mutable.Map.empty
  private val fieldCache: mutable.Map[String, Int] = mutable.Map.empty
  private var _fieldNames: Seq[String] = Seq.empty

  def buildCache()(implicit constantPool: ConstantPool): Unit = {
    if (cacheBuilt)
      return

    fieldCache.addAll(fields.zipWithIndex.collect({ case (ConstantSlot(ResolveConstantRef(ConstantString(name))), i) =>
      (name -> i)
    }))
    _fieldNames = fields.collect({ case ConstantSlot(ResolveConstantRef(ConstantString(name))) => name })

    methodCache.addAll(methods.collect({ case method@ConstantMethod(ResolveConstantRef(ConstantString(name)), _, _, _, _, _) =>
      (name -> method)
    }))

    cacheBuilt = true
  }

  def fieldNames: Seq[String] = _fieldNames

  def getMethod(name: String): Option[ConstantMethod] = methodCache.get(name)
  def getFieldIndex(name: String): Int = fieldCache.getOrElse(name, -1)

}

object ConstantClass {
  val tag: Int = 0x05
}

case class ConstantRef[+T <: ProgramObject](index: Int) {
  def resolve(implicit constantPool: ConstantPool): T = constantPool(index).asInstanceOf[T]

  def resolveOption[R >: T : ClassTag](implicit constantPool: ConstantPool): Option[R] =
    Some(constantPool(index)).collect { case v: R => v }
}

object ResolveConstantRef {
  def unapply[T <: ProgramObject : ClassTag](arg: ConstantRef[T])(implicit constantPool: ConstantPool): Option[T] = arg.resolveOption
}

object ResolveConstantRefFast {
  def unapply[T <: ProgramObject : ClassTag](arg: ConstantRef[T])(implicit constantPool: ConstantPool): Option[T] = Some(arg.resolve)
}

case class Program(code: ByteBuffer, constantPool: ConstantPool, globalRefs: IndexedSeq[ConstantRef[ConstantMember]], entryRef: ConstantRef[ConstantMethod]) {
  assert(code.order() == ByteOrder.LITTLE_ENDIAN)

  private implicit val _program: Program = this
  private implicit val _constantPool: ConstantPool = constantPool

  def getOpcode(loc: ProgramLoc): Int = code.getUnsigned(loc)

  def labelMap: Map[String, ProgramLoc] = {
    constantPool.collect { case m: ConstantMethod => m.codeIterator(this).map(_._1).collect {
      case loc@OpLabel(ResolveConstantRef(ConstantString(name))) => (name -> loc)
    }
    }.flatten.toMap
  }
}
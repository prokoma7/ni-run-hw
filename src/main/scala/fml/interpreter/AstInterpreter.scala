package fml.interpreter

import fml.ast._
import fml.state._

import scala.annotation.tailrec
import scala.collection.mutable


class ObjectDefPool {

  private val defsById: mutable.Buffer[ObjectDef] = new mutable.ArrayBuffer
  private val defIds: mutable.Map[ObjectDef, Int] = new mutable.HashMap

  def allocateObjectId(objectDef: ObjectDef): Int = defIds.getOrElseUpdate(objectDef, {
    val id = defsById.size
    defsById.addOne(objectDef)
    id
  })

  def apply(objectId: Int): ObjectDef = defsById(objectId)
}

class AstInterpreter(protected val _heap: Heap) extends Interpreter {
  val env = new ExecutionEnv

  override protected implicit def heap: Heap = _heap

  val funDefs: mutable.Map[String, FunctionDef] = new mutable.HashMap
  val objDefs: ObjectDefPool = new ObjectDefPool

  case class AstClassDef(that: ObjectDef) extends ClassDef {
    override def fieldNames: Seq[String] = that.fields.map(_.name)
  }

  override protected def resolveClassId(classId: ClassId): AstClassDef = AstClassDef(objDefs(classId))

  def eval(node: AstNode): HeapElementRef = node match {
    case IntLiteral(value) => heap.allocateInt(value)
    case BooleanLiteral(value) => heap.allocateBoolean(value)
    case NullLiteral() => heap.allocateNull()
    case AccessVariable(name) => env.lookupVar(name) match {
      case Some(ptr) => ptr
      case None => throw ExecutionException(s"""AccessVariable: Lookup of "$name" failed.""")
    }
    case AssignVariable(name, value) => {
      val valueRef = eval(value)
      env.lookupVar(name) match {
        case Some(_) => env.updateVar(name, valueRef)
        case None => throw ExecutionException(s"""AssignVariable: Lookup of "$name" failed.""")
      }
      valueRef
    }
    case VariableDef(name, value) => {
      val valueRef = eval(value)
      env.allocateVar(name, valueRef)
      valueRef
    }
    case ArrayDef(size, value) => {
      val sizeVal = evalInt(size)
      val ptr = heap.allocateArray(0.until(sizeVal).map(_ => eval(value)))
      ptr
    }
    case AccessArray(array, index) => {
      evalCallMethod(eval(array), "get", Seq(eval(index)))
    }
    case AssignArray(array, index, value) => {
      evalCallMethod(eval(array), "set", Seq(eval(index), eval(value)))
    }
    case funDef: FunctionDef => {
      funDefs.put(funDef.name, funDef)
      heap.allocateNull()
    }
    case CallFunction(name, arguments) => {
      val funDef = funDefs(name)
      if(funDef.parameters.size != arguments.size)
        throw ExecutionException(s"CallFunction: Function $name expects ${funDef.parameters.size} parameters, ${arguments.size} given.")

      val argRefs = arguments.map(eval)
      env.withFrame(() => {
        for ((param, valueRef) <- funDef.parameters.zip(argRefs))
          env.allocateVar(param, valueRef)
        eval(funDef.body)
      }, extendGlobalScope = true)
    }
    case Print(format, arguments) => {
      evalPrint(format, arguments.map(eval))
      heap.allocateNull()
    }
    case Top(children) => {
      children.foldLeft(heap.allocateNull())((_, node) => eval(node))
    }
    case Block(children) => {
      env.withFrame(() => {
        children.foldLeft(heap.allocateNull())((_, node) => eval(node))
      })
    }
    case Loop(condition, body) => {
      while(evalCondition(condition))
        eval(body)
      heap.allocateNull()
    }
    case Conditional(condition, consequent, alternative) => {
      if(evalCondition(condition))
        eval(consequent)
      else
        eval(alternative)
    }
    case objDef: ObjectDef => {
      val parentRef = eval(objDef.parent)
      val objectId = objDefs.allocateObjectId(objDef)
      heap.allocateObject(objectId, parentRef, objDef.members.collect {
        case VariableDef(name, value) => eval(value)
      })
    }
    case AssignField(obj, field, value) => {
      val objRef = eval(obj)
      val valueRef = eval(value)
      resolveObjectFieldIndex(objRef, field) match {
        case (realObjRef, fieldIndex) => heap.putObjectField(realObjRef, fieldIndex, valueRef)
      }
      valueRef
    }
    case AccessField(obj, field) => {
      val objRef = eval(obj)
      resolveObjectFieldIndex(objRef, field) match {
        case (realObjRef, fieldIndex) => heap.tryGetObjectField(realObjRef, fieldIndex).get
      }
    }
    case CallMethod(obj, name, arguments) => evalCallMethod(eval(obj), name, arguments.map(eval))
  }

  @tailrec
  private def evalCallMethod(objRef: HeapElementRef, name: String, argRefs: Seq[HeapElementRef]): HeapElementRef = objRef match {
    case HeapInt(_) | HeapBoolean(_) | HeapNull() => evalCallMethodPrimitive(objRef, name, argRefs)
    case HeapArray(_) => evalCallMethodArray(objRef, name, argRefs)
    case HeapObject(objectId, parentRef, _) => {
      val objectDef = objDefs(objectId)
      objectDef.members.collectFirst { case funDef: FunctionDef if funDef.name == name => funDef } match {
        case Some(funDef) => {
          if(funDef.parameters.size != argRefs.size)
            throw ExecutionException(s"CallMethod: Method $name expects ${funDef.parameters.size} parameters, ${argRefs.size} given.")

          env.withFrame(() => {
            env.allocateVar("this", objRef)
            for ((param, valueRef) <- funDef.parameters.zip(argRefs))
              env.allocateVar(param, valueRef)
            eval(funDef.body)
          }, extendGlobalScope = true)
        }
        case None => evalCallMethod(parentRef, name, argRefs)
      }
    }
  }

  private def evalCondition(node: AstNode): Boolean = isRefTruthy(eval(node))

  private def evalInt(node: AstNode): Int = {
    eval(node) match {
      case HeapInt(value) => value
      case ptr => throw ExecutionException(s"Expected HeapInt, got ${HeapElement(heap.getTypeId(ptr))}")
    }
  }
}
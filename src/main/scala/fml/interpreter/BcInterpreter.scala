package fml.interpreter

import fml.bc._
import fml.interpreter.BcInterpreter.HALT
import fml.state._

import scala.annotation.tailrec
import scala.collection.mutable

class BcInterpreter(private val _program: Program, protected val _heap: ManagedHeap) extends Interpreter {
  type CallStack = mutable.Stack[StackFrame]
  type OperandStack = mutable.Stack[HeapElementRef]

  case class StackFrame(retLoc: ProgramLoc, codeEndLoc: ProgramLoc, locals: mutable.IndexedSeq[HeapElementRef])

  class InsnPointer(var loc: ProgramLoc) {
    def opcode: Int = program.getOpcode(loc)

    @inline
    def bump(by: Int): Unit = loc += by
    def bump(by: Insn): Unit = bump(by.length)

    def set(loc: ProgramLoc): Unit = this.loc = loc
  }

  implicit class HeapOps(that: Heap) {
    def allocateConstantLiteral(literal: ConstantLiteral): HeapElementRef = literal match {
      case ConstantInt(value) => that.allocateInt(value)
      case ConstantBoolean(value) => that.allocateBoolean(value)
      case ConstantNull() => that.allocateNull()
    }
  }

  override protected implicit def heap: ManagedHeap = _heap
  private implicit def program: Program = _program
  private implicit def constantPool: ConstantPool = program.constantPool

  constantPool.collect({ case cl: ConstantClass => cl.buildCache() })

  val globals: mutable.Map[String, HeapElementRef] = mutable.Map.from(program.globalRefs.collect {
    case ResolveConstantRef(ConstantSlot(ResolveConstantRef(ConstantString(name)))) =>
      (name -> heap.allocateNull())
  })

  val globalFuns: Map[String, ConstantMethod] = program.globalRefs.collect {
    case ResolveConstantRef(m@ConstantMethod(ResolveConstantRef(ConstantString(name)), _, _, _, _, _)) =>
      (name -> m)
  }.toMap

  val callStack: CallStack = new CallStack
  val operandStack: OperandStack = new OperandStack
  val insnPointer: InsnPointer = new InsnPointer(HALT)
  val labelMap: Map[String, ProgramLoc] = program.labelMap

  case class GlobalGcRoot(name: String) extends GcRoot {
    override def ref: HeapElementRef = globals(name)

    override def replace(newRef: HeapElementRef): Unit = globals(name) = newRef
  }
  case class LocalGcRoot(frame: StackFrame, index: Int) extends GcRoot {
    override def ref: HeapElementRef = frame.locals(index)

    override def replace(newRef: HeapElementRef): Unit = frame.locals(index) = newRef
  }
  case class OperandGcRoot(index: Int) extends GcRoot {
    override def ref: HeapElementRef = operandStack(index)

    override def replace(newRef: HeapElementRef): Unit = operandStack(index) = newRef
  }

  heap.gcRootSupplier = Some(() => Iterator.concat(
    globals.keys.map(GlobalGcRoot),
    callStack.flatMap(frame => frame.locals.indices.map(LocalGcRoot(frame, _))),
    operandStack.indices.map(OperandGcRoot)
  ))

  case class BcClassDef(that: ConstantClass) extends ClassDef {
    override def fieldNames: Seq[String] = that.fieldNames

    override def getFieldIndex(fieldName: String): ClassId = that.getFieldIndex(fieldName)
  }

  override protected def resolveClassId(classId: ClassId): BcClassDef = BcClassDef(constantPool(classId).asInstanceOf[ConstantClass])

  private def step(): Unit = insnPointer.opcode match {
    case OpLiteral.opcode => insnPointer.loc match {
      case OpLiteral(ResolveConstantRefFast(value)) =>
        operandStack.push(heap.allocateConstantLiteral(value))
        insnPointer.bump(OpLiteral.length)
    }
    case OpGetLocal.opcode => insnPointer.loc match {
      case OpGetLocal(index) =>
        operandStack.push(callStack.top.locals(index))
        insnPointer.bump(OpGetLocal.length)
    }
    case OpSetLocal.opcode => insnPointer.loc match {
      case OpSetLocal(index) =>
        callStack.top.locals(index) = operandStack.top
        insnPointer.bump(OpSetLocal.length)
    }
    case OpGetGlobal.opcode => insnPointer.loc match {
      case OpGetGlobal(ResolveConstantRefFast(ConstantString(name))) =>
        if(!globals.contains(name))
          throw ExecutionException(s"GetGlobal: Missing global $name.")

        operandStack.push(globals(name))
        insnPointer.bump(OpGetGlobal.length)
    }
    case OpSetGlobal.opcode => insnPointer.loc match {
      case OpSetGlobal(ResolveConstantRefFast(ConstantString(name))) =>
        if(!globals.contains(name))
          throw ExecutionException(s"SetGlobal: Missing global $name.")

        globals(name) = operandStack.top
        insnPointer.bump(OpSetGlobal.length)
    }
    case OpCallFunction.opcode => insnPointer.loc match {
      case OpCallFunction(ResolveConstantRefFast(ConstantString(name)), arguments) =>
        var argRefs = List.empty[HeapElementRef]
        var i = 0
        while(i < arguments) {
          argRefs = argRefs.prepended(operandStack.pop())
          i += 1
        }
        val funDef = globalFuns(name)
        if(funDef.arguments != arguments)
          throw ExecutionException(s"CallFunction: Function $name expects ${funDef.arguments} parameters, $arguments given.")
        insnPointer.bump(OpCallFunction.length)
        evalCallFunction(funDef, argRefs)
    }
    case OpReturn.opcode => evalReturn()
    case OpLabel.opcode => insnPointer.bump(OpLabel.length)
    case OpJump.opcode => insnPointer.loc match {
      case OpJump(ResolveConstantRefFast(ConstantString(label))) =>
        insnPointer.set(labelMap.getOrElse(label, throw ExecutionException(s"Jump: Missing label $label.")))
    }
    case OpBranch.opcode => insnPointer.loc match {
      case OpBranch(ResolveConstantRefFast(ConstantString(label))) =>
        val condRef = operandStack.pop()
        if(isRefTruthy(condRef))
          insnPointer.set(labelMap.getOrElse(label, throw ExecutionException(s"Branch: Missing label $label.")))
        else
          insnPointer.bump(OpBranch.length)
    }
    case OpPrint.opcode => insnPointer.loc match {
      case OpPrint(ResolveConstantRefFast(ConstantString(format)), numArgs) =>
        val argRefs = 0.until(numArgs).map(_ => operandStack.pop()).reverse
        evalPrint(format, argRefs)
        operandStack.push(heap.allocateNull())
        insnPointer.bump(OpPrint.length)
    }
    case OpArray.opcode =>
      val initialValue = operandStack.pop()
      operandStack.pop() match {
        case HeapInt(length) if length >= 0 =>
          operandStack.push(heap.allocateArray(0.until(length).map(_ => initialValue)))
          insnPointer.bump(OpArray.length)
        case lengthRef => throw ExecutionException(s"Array: Expected non-negative integer length, ${refToString(lengthRef)} given.")
      }
    case OpObject.opcode => insnPointer.loc match {
      case OpObject(classId@ResolveConstantRefFast(c: ConstantClass)) =>
        val fieldValueRefs = c.fieldNames.map(_ => operandStack.pop()).reverse
        val parentRef = operandStack.pop()

        operandStack.push(heap.allocateObject(classId.index, parentRef, fieldValueRefs))
        insnPointer.bump(OpObject.length)
    }
    case OpGetField.opcode => insnPointer.loc match {
      case OpGetField(ResolveConstantRefFast(ConstantString(field))) =>
        val objRef = operandStack.pop()
        resolveObjectFieldIndex(objRef, field) match {
          case (realObjRef, fieldIndex) => operandStack.push(heap.tryGetObjectField(realObjRef, fieldIndex).get)
        }
        insnPointer.bump(OpGetField.length)
    }
    case OpSetField.opcode => insnPointer.loc match {
      case OpSetField(ResolveConstantRefFast(ConstantString(field))) =>
        val valRef = operandStack.pop()
        val objRef = operandStack.pop()
        resolveObjectFieldIndex(objRef, field) match {
          case (realObjRef, fieldIndex) => heap.putObjectField(realObjRef, fieldIndex, valRef)
        }
        operandStack.push(valRef)
        insnPointer.bump(OpSetField.length)
    }
    case OpCallMethod.opcode => insnPointer.loc match {
      case OpCallMethod(ResolveConstantRefFast(ConstantString(name)), numArgs) =>
        var argRefs = List.empty[HeapElementRef]
        var i = 0
        while(i < numArgs - 1) {
          argRefs = argRefs.prepended(operandStack.pop())
          i += 1
        }
        val objRef = operandStack.pop()
        insnPointer.bump(OpCallMethod.length)
        evalCallMethod(objRef, name, argRefs)
    }
    case OpDrop.opcode =>
      operandStack.pop()
      insnPointer.bump(OpDrop.length)
  }

  private def evalCallFunction(funDef: ConstantMethod, argRefs: Seq[HeapElementRef]): Unit = {
    val locals = mutable.ArrayBuffer.from(argRefs)
    var i = funDef.locals
    while(i > 0) {
      locals.addOne(HeapNull.nullPtr)
      i -= 1
    }

    callStack.push(StackFrame(insnPointer.loc, funDef.codeEndLoc, locals))
    insnPointer.set(funDef.codeStartLoc)
  }

  @tailrec
  private def evalCallMethod(objRef: HeapElementRef, name: String, argRefs: Seq[HeapElementRef]): Unit = heap.getTypeId(objRef) match {
    case HeapInt.typeId => operandStack.push(evalCallMethodInt1(objRef, name, argRefs.head))
    case HeapBoolean.typeId | HeapNull.typeId => operandStack.push(evalCallMethodPrimitive(objRef, name, argRefs))
    case HeapArray.typeId => operandStack.push(evalCallMethodArray(objRef, name, argRefs))
    case _ => objRef match {
      case HeapObject(classId, parentRef, _) =>
        val classDef = constantPool(classId).asInstanceOf[ConstantClass]
        classDef.getMethod(name) match {
          case Some(funDef) =>
            if (funDef.arguments - 1 != argRefs.size)
              throw ExecutionException(s"CallMethod: Method $name expects ${funDef.arguments - 1} parameters, ${argRefs.size} given.")
            evalCallFunction(funDef, argRefs.prepended(objRef))
          case None => evalCallMethod(parentRef, name, argRefs)
        }
    }
  }

  private def evalReturn(): Unit = {
    val frame = callStack.pop()
    insnPointer.set(frame.retLoc)
  }

  @inline
  private def isHalted: Boolean = {
    while(insnPointer.loc != HALT && insnPointer.loc == callStack.top.codeEndLoc)
      evalReturn()
    insnPointer.loc == HALT
  }

  def run(): Unit = {
    val entryMethod = program.entryRef.resolve
    evalCallFunction(entryMethod, Seq.empty)

    while(!isHalted)
      step()
  }
}

object BcInterpreter {
  val HALT: ProgramLoc = 0
}
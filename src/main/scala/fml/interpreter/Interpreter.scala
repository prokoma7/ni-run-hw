package fml.interpreter

import fml.state._

import scala.annotation.tailrec

case class ExecutionException(message: String) extends RuntimeException(message)

trait ClassDef {
  def fieldNames: Seq[String]

  def getFieldIndex(fieldName: String): Int = fieldNames.indexOf(fieldName)
}

trait Interpreter {
  protected implicit def heap: Heap

  protected def resolveClassId(classId: ClassId): ClassDef

  protected def resolveObjectFieldIndex(objRef: HeapElementRef, fieldName: String): (HeapElementRef, Int) = {
    @tailrec
    def impl(curRef: HeapElementRef): (HeapElementRef, Int) = curRef match {
      case HeapObject(classId, parentRef, _) => {
        val fieldIndex = resolveClassId(classId).getFieldIndex(fieldName)
        if (fieldIndex != -1)
          (curRef, fieldIndex)
        else
          impl(parentRef)
      }
      case _ => throw ExecutionException(s"Couldn't access $fieldName on ${refToString(objRef)}")
    }

    impl(objRef)
  }

  protected def evalCallMethodInt1(objRef: HeapElementRef, name: String, argRef: HeapElementRef): HeapElementRef = {
    val obj = heap.tryGetInt(objRef).get
    heap.tryGetInt(argRef) match {
      case Some(arg) => name match {
        case "+" | "add" => heap.allocateInt(obj + arg)
        case "-" | "sub" => heap.allocateInt(obj - arg)
        case "*" | "mul" => heap.allocateInt(obj * arg)
        case "/" | "div" => heap.allocateInt(obj / arg)
        case "%" | "mod" => heap.allocateInt(obj % arg)
        case "<=" | "le" => heap.allocateBoolean(obj <= arg)
        case ">=" | "ge" => heap.allocateBoolean(obj >= arg)
        case "<" | "lt" => heap.allocateBoolean(obj < arg)
        case ">" | "gt" => heap.allocateBoolean(obj > arg)
        case "==" | "eq" => heap.allocateBoolean(obj == arg)
        case "!=" | "neq" => heap.allocateBoolean(obj != arg)
      }
      case _ => name match {
        case "==" | "eq" => heap.allocateBoolean(false)
        case "!=" | "neq" => heap.allocateBoolean(true)
      }
    }
  }

  protected def evalCallMethodPrimitive(objRef: HeapElementRef, name: String, argRefs: Seq[HeapElementRef]): HeapElementRef = (objRef, name, argRefs) match {
//    case (HeapInt(value), "+" | "add", Seq(HeapInt(arg))) => heap.allocateInt(value + arg)
//    case (HeapInt(value), "-" | "sub", Seq(HeapInt(arg))) => heap.allocateInt(value - arg)
//    case (HeapInt(value), "*" | "mul", Seq(HeapInt(arg))) => heap.allocateInt(value * arg)
//    case (HeapInt(value), "/" | "div", Seq(HeapInt(arg))) => heap.allocateInt(value / arg)
//    case (HeapInt(value), "%" | "mod", Seq(HeapInt(arg))) => heap.allocateInt(value % arg)
//    case (HeapInt(value), "<=" | "le", Seq(HeapInt(arg))) => heap.allocateBoolean(value <= arg)
//    case (HeapInt(value), ">=" | "ge", Seq(HeapInt(arg))) => heap.allocateBoolean(value >= arg)
//    case (HeapInt(value), "<" | "lt", Seq(HeapInt(arg))) => heap.allocateBoolean(value < arg)
//    case (HeapInt(value), ">" | "gt", Seq(HeapInt(arg))) => heap.allocateBoolean(value > arg)
//    case (HeapInt(value), "==" | "eq", Seq(HeapInt(arg))) => heap.allocateBoolean(value == arg)
//    case (HeapInt(value), "!=" | "neq", Seq(HeapInt(arg))) => heap.allocateBoolean(value != arg)

    case (HeapBoolean(value), "&" | "and", Seq(HeapBoolean(arg))) => heap.allocateBoolean(value && arg)
    case (HeapBoolean(value), "|" | "or", Seq(HeapBoolean(arg))) => heap.allocateBoolean(value || arg)
    case (HeapBoolean(value), "==" | "eq", Seq(HeapBoolean(arg))) => heap.allocateBoolean(value == arg)
    case (HeapBoolean(value), "!=" | "neq", Seq(HeapBoolean(arg))) => heap.allocateBoolean(value != arg)

    case (ptr, "==" | "eq", Seq(ptr2)) => heap.allocateBoolean(ptr == ptr2)
    case (ptr, "!=" | "neq", Seq(ptr2)) => heap.allocateBoolean(ptr != ptr2)

    case (ptr, op, args) => throw ExecutionException(s"${HeapElement(heap.getTypeId(ptr))} doesn't support $op with arguments [${args.map(refToString).mkString(",")}]")
  }

  def evalCallMethodArray(objRef: HeapElementRef, name: String, argRefs: Seq[HeapElementRef]): HeapElementRef = (objRef, name, argRefs) match {
    case (HeapArray(size), "get", Seq(HeapInt(indexVal))) => {
      if (indexVal < 0 || indexVal >= size)
        throw ExecutionException(s"Array index $indexVal out of bounds (size: $size)")
      heap.getArrayElement(objRef, indexVal)
    }
    case (HeapArray(size), "set", Seq(HeapInt(indexVal), valueRef)) => {
      if (indexVal < 0 || indexVal >= size)
        throw ExecutionException(s"Array index $indexVal out of bounds (size: $size)")
      heap.putArrayElement(objRef, indexVal, valueRef)
      valueRef
    }

    case (HeapArray(_), op, args) => throw ExecutionException(s"HeapArray doesn't support $op with arguments [${args.map(refToString).mkString(",")}]")
  }

  protected def evalPrint(format: String, argRefs: Seq[HeapElementRef]): Unit = {
    var remainingArgs = argRefs
    val sb = new StringBuilder
    var esc = false
    format.foreach(c => if(esc) {
      sb.append(c match {
        case '~' => '~'
        case 'n' => '\n'
        case '\\' => '\\'
        case 'r' => '\r'
        case 't' => '\t'
        case '\"' => '\"'
        case _ => throw ExecutionException(s"Unknown escape sequence: \\\\$c")
      })
      esc = false
    } else {
      c match {
        case '\\' => esc = true
        case '~' =>
          sb.append(refToString(remainingArgs.head))
          remainingArgs = remainingArgs.tail
        case _ => sb.append(c)
      }
    })
    printStr(sb.toString())
  }

  protected def isRefTruthy(ref: HeapElementRef): Boolean = ref match {
    case HeapBoolean(false) | HeapNull() => false
    case _ => true
  }

  protected def refToString(ref: HeapElementRef): String = ref match {
    case HeapNull() => "null"
    case HeapBoolean(value) => if (value) "true" else "false"
    case HeapInt(value) => value.toString
    case HeapArray(size) => "[" + 0.until(size).map(i => refToString(heap.tryGetArrayElement(ref, i).get)).mkString(", ") + "]"
    case HeapObject(classId, parentRef, _) => {
      val classDef = resolveClassId(classId)
      val fieldValues = classDef.fieldNames.zipWithIndex.sortBy(_._1).map { case (fieldName, index) =>
        fieldName + "=" + refToString(heap.tryGetObjectField(ref, index).get)
      }
      parentRef match {
        case HeapNull() => "object(" + fieldValues.mkString(", ") + ")"
        case _ => "object(" + (("..=" + refToString(parentRef)) +: fieldValues).mkString(", ") + ")"
      }
    }
  }

  protected def printStr(str: String): Unit = Console.print(str)

}

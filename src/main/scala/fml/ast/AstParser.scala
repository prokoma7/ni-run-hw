package fml.ast

import java.io.Reader

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.deser.std.DelegatingDeserializer
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.{BeanDescription, DeserializationConfig, DeserializationContext, JsonDeserializer}
import com.fasterxml.jackson.module.scala.DefaultScalaModule

private class AstNodeDeserializer(d: JsonDeserializer[_]) extends DelegatingDeserializer(d) {
  override def newDelegatingInstance(newDelegatee: JsonDeserializer[_]): JsonDeserializer[_] = new AstNodeDeserializer(newDelegatee)

  override def deserialize(p: JsonParser, ctxt: DeserializationContext): AstNode = {
    if(p.getValueAsString() == "Null")
      NullLiteral()
    else
      super.deserialize(p, ctxt).asInstanceOf[AstNode]
  }

  override def deserializeWithType(p: JsonParser, ctxt: DeserializationContext, typeDeserializer: TypeDeserializer): AstNode = {
    if(p.getValueAsString() == "Null")
      NullLiteral()
    else
      super.deserializeWithType(p, ctxt, typeDeserializer).asInstanceOf[AstNode]
  }
}

object AstParser {
  private val astParserModule = new SimpleModule
  astParserModule.addDeserializer(classOf[Top], (p, _) => {
    Top(p.readValueAs(new TypeReference[Seq[AstNode]] {}))
  })
  astParserModule.addDeserializer(classOf[Block], (p, _) => {
    Block(p.readValueAs(new TypeReference[Seq[AstNode]] {}))
  })
  astParserModule.addDeserializer(classOf[IntLiteral], (p, _) => IntLiteral(p.readValueAs(classOf[Int])))
  astParserModule.addDeserializer(classOf[BooleanLiteral], (p, _) => BooleanLiteral(p.readValueAs(classOf[Boolean])))

  astParserModule.setDeserializerModifier(new BeanDeserializerModifier {
    override def modifyDeserializer(config: DeserializationConfig, beanDesc: BeanDescription, deserializer: JsonDeserializer[_]): JsonDeserializer[_] = {
      val defaultDeserializer = super.modifyDeserializer(config, beanDesc, deserializer)
      if(classOf[AstNode].isAssignableFrom(beanDesc.getBeanClass)) {
        new AstNodeDeserializer(defaultDeserializer)
      } else
        defaultDeserializer
    }
  })

  private val mapper = JsonMapper.builder()
    .addModule(DefaultScalaModule)
    .addModule(astParserModule)
    .build()

  def parse(reader: Reader): AstNode = mapper.readValue(reader, classOf[AstNode])
}


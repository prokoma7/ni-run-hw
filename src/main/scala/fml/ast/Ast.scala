package fml.ast

import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.{JsonFormat, JsonProperty, JsonSubTypes, JsonTypeInfo}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes(Array(
  new Type(value = classOf[IntLiteral], name = "Integer"),
  new Type(value = classOf[BooleanLiteral], name = "Boolean"),
  new Type(value = classOf[NullLiteral], name = "Null"),
  new Type(value = classOf[AccessVariable], name = "AccessVariable"),
  new Type(value = classOf[AssignVariable], name = "AssignVariable"),
  new Type(value = classOf[VariableDef], name = "Variable"),
  new Type(value = classOf[ArrayDef], name = "Array"),
  new Type(value = classOf[AccessArray], name = "AccessArray"),
  new Type(value = classOf[AssignArray], name = "AssignArray"),
  new Type(value = classOf[FunctionDef], name = "Function"),
  new Type(value = classOf[CallFunction], name = "CallFunction"),
  new Type(value = classOf[Print], name = "Print"),
  new Type(value = classOf[Block], name = "Block"),
  new Type(value = classOf[Top], name = "Top"),
  new Type(value = classOf[Loop], name = "Loop"),
  new Type(value = classOf[Conditional], name = "Conditional"),
  new Type(value = classOf[ObjectDef], name = "Object"),
  new Type(value = classOf[AssignField], name = "AssignField"),
  new Type(value = classOf[AccessField], name = "AccessField"),
  new Type(value = classOf[CallMethod], name = "CallMethod"),
))
sealed abstract class AstNode {
  def children: Seq[AstNode] = Seq.empty
}

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
case class IntLiteral(value: Int) extends AstNode

@JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
case class BooleanLiteral(value: Boolean) extends AstNode

case class NullLiteral() extends AstNode

case class AccessVariable(name: String) extends AstNode

case class AssignVariable(name: String, value: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(value)
}

case class VariableDef(name: String, value: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(value)
}

case class ArrayDef(size: AstNode, value: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(size, value)
}

case class AccessArray(array: AstNode, index: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(array, index)
}

case class AssignArray(array: AstNode, index: AstNode, value: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(array, index, value)
}

case class FunctionDef(name: String, parameters: Seq[String], body: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(body)
}

case class CallFunction(name: String, arguments: Seq[AstNode]) extends AstNode {
  override def children: Seq[AstNode] = arguments
}

case class Print(format: String, arguments: Seq[AstNode]) extends AstNode {
  override def children: Seq[AstNode] = arguments
}

case class Loop(condition: AstNode, body: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(condition, body)
}

case class Conditional(condition: AstNode, consequent: AstNode, alternative: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(condition, consequent, alternative)
}

case class ObjectDef(@JsonProperty("extends") parent: AstNode, members: Seq[AstNode]) extends AstNode {
  override def children: Seq[AstNode] = parent +: members

  def fields: Seq[VariableDef] = members collect { case d: VariableDef => d }

  def methods: Seq[FunctionDef] = members collect { case d: FunctionDef => d }
}

case class AssignField(@JsonProperty("object") obj: AstNode, field: String, value: AstNode) extends AstNode {
  override def children: Seq[AstNode] = Seq(obj, value)
}

case class AccessField(@JsonProperty("object") obj: AstNode, field: String) extends AstNode {
  override def children: Seq[AstNode] = Seq(obj)
}

case class CallMethod(@JsonProperty("object") obj: AstNode, name: String, arguments: Seq[AstNode]) extends AstNode {
  override def children: Seq[AstNode] = obj +: arguments
}

sealed trait BlockBase extends AstNode

case class Block(override val children: Seq[AstNode]) extends BlockBase

case class Top(override val children: Seq[AstNode]) extends BlockBase
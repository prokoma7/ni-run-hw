package fml.compiler

import fml.ast._
import fml.bc._

import scala.collection.mutable

case class CompilationException(message: String) extends RuntimeException(message)

class AstToBcCompiler {
  import AstToBcCompiler._

  val programBuilder: ProgramBuilder = new ProgramBuilder with ConstantPoolDeduplication

  val env: CompilationEnv = new CompilationEnv
  val funDefs: mutable.Map[String, FunctionDef] = mutable.Map.empty

  def compile(node: AstNode): ProgramSerializable = node match {
    case Top(children) =>
      compileGlobalDecls(node)
      val methodBuilder = programBuilder.newMethodBuilder("λ:", 0)
      compileBlockChildren(methodBuilder, children)
      programBuilder.setEntryRef(programBuilder.append(methodBuilder))
      programBuilder

    case _ => throw CompilationException(s"Expected Top, got $node")
  }

  /** Collect global variable and function declarations. */
  def compileGlobalDecls(node: AstNode): Unit = node match {
    case VariableDef(name, value) =>
      compileGlobalVariableDecl(name)
      compileGlobalDecls(value)

    case funDef@FunctionDef(name, _, _) =>
      if(funDefs.contains(name))
        throw CompilationException(s"FunctionDef: Redefinition of function '$name'.")
      funDefs.put(name, funDef)

    case ObjectDef(parent, members) =>
      compileGlobalDecls(parent)
      members.foreach {
        case VariableDef(_, value) => compileGlobalDecls(value)
        case _ =>
      }

    case _: Block =>

    case _ => node.children.foreach(compileGlobalDecls)
  }

  /** After each compileExpr there is exactly one new element on the stack. */
  def compileExpr(methodBuilder: ConstantMethodBuilder, node: AstNode): Unit = node match {
    case IntLiteral(value) =>
      methodBuilder.append(OpLiteral(programBuilder.appendInt(value)))
    case BooleanLiteral(value) =>
      methodBuilder.append(OpLiteral(programBuilder.appendBoolean(value)))
    case NullLiteral() =>
      methodBuilder.append(OpLiteral(programBuilder.appendNull()))

    case AccessVariable(name) => env.lookupVar(name) match {
      case Some(access) => methodBuilder.append(access.getInsn)
      case None => throw CompilationException(s"AccessVariable: Lookup of '$name' failed.")
    }

    case AssignVariable(name, value) =>
      compileExpr(methodBuilder, value)
      env.lookupVar(name) match {
        case Some(access) => methodBuilder.append(access.setInsn)
        case None => throw CompilationException(s"AssignVariable: Lookup of '$name' failed.")
      }

    case VariableDef(name, value) =>
      compileExpr(methodBuilder, value)
      val access = if(env.isGlobal) env.lookupVar(name).get else {
        val index = methodBuilder.appendLocal()
        env.allocateVar(name, VarAccess(OpGetLocal(index), OpSetLocal(index)))
      }
      methodBuilder.append(access.setInsn)

    case ArrayDef(size, value) =>
      compileArrayDef(methodBuilder, size, value)

    case AccessArray(array, index) =>
      compileCallMethod(methodBuilder, array, "get", Seq(index))

    case AssignArray(array, index, value) =>
      compileCallMethod(methodBuilder, array, "set", Seq(index, value))

    case FunctionDef(name, parameters, body) =>
      programBuilder.appendGlobalRef(compileMethodDef(name, parameters, body))
      methodBuilder.append(OpLiteral(programBuilder.appendNull()))

    case CallFunction(name, arguments) =>
      val funDef = funDefs.getOrElse(name, throw CompilationException(s"CallFunction: Lookup of '$name' failed."))
      if (funDef.parameters.size != arguments.size)
        throw CompilationException(s"CallFunction: Function '$name' expects ${funDef.parameters.size} parameters, ${arguments.size} given.")

      arguments.foreach(arg => compileExpr(methodBuilder, arg))
      methodBuilder.append(OpCallFunction(programBuilder.appendString(name), arguments.size))

    case Print(format, arguments) =>
      arguments.foreach(arg => compileExpr(methodBuilder, arg))
      methodBuilder.append(OpPrint(programBuilder.appendString(format), arguments.size))

    case Top(_) => throw CompilationException(s"Top: Must be at the root of the AST.")

    case Block(children) =>
      env.withFrame {
        compileBlockChildren(methodBuilder, children)
      }

    case Loop(condition, body) =>
      val condLabelNameRef = programBuilder.appendLabelName("loopCond")
      val bodyLabelNameRef = programBuilder.appendLabelName("loopBody")
      val endLabelNameRef = programBuilder.appendLabelName("loopEnd")
      methodBuilder.append(OpLabel(condLabelNameRef))
      compileExpr(methodBuilder, condition)
      methodBuilder.append(OpBranch(bodyLabelNameRef))
      methodBuilder.append(OpJump(endLabelNameRef))
      methodBuilder.append(OpLabel(bodyLabelNameRef))
      compileExpr(methodBuilder, body)
      methodBuilder.append(OpDrop())
      methodBuilder.append(OpJump(condLabelNameRef))
      methodBuilder.append(OpLabel(endLabelNameRef))
      methodBuilder.append(OpLiteral(programBuilder.appendNull()))

    case Conditional(condition, consequent, alternative) =>
      val consLabelName = programBuilder.appendLabelName("condCons")
      val endLabelName = programBuilder.appendLabelName("condEnd")
      compileExpr(methodBuilder, condition)
      methodBuilder.append(OpBranch(consLabelName))
      compileExpr(methodBuilder, alternative)
      methodBuilder.append(OpJump(endLabelName))
      methodBuilder.append(OpLabel(consLabelName))
      compileExpr(methodBuilder, consequent)
      methodBuilder.append(OpLabel(endLabelName))

    case ObjectDef(parent, members) =>
      val memberRefs = members.collect {
        case VariableDef(name, _) => programBuilder.appendSlot(name)
        case FunctionDef(name, parameters, body) => compileMethodDef(name, "this" +: parameters, body)
      }.toIndexedSeq
      val classRef = programBuilder.append(ConstantClass(memberRefs))

      compileExpr(methodBuilder, parent)
      members.foreach {
        case VariableDef(_, value) => compileExpr(methodBuilder, value)
        case _ =>
      }
      methodBuilder.append(OpObject(classRef))

    case AssignField(obj, field, value) =>
      compileExpr(methodBuilder, obj)
      compileExpr(methodBuilder, value)
      methodBuilder.append(OpSetField(programBuilder.appendString(field)))

    case AccessField(obj, field) =>
      compileExpr(methodBuilder, obj)
      methodBuilder.append(OpGetField(programBuilder.appendString(field)))

    case CallMethod(obj, name, arguments) =>
      compileCallMethod(methodBuilder, obj, name, arguments)
  }

  def compileGlobalVariableDecl(name: String): VarAccess = {
    val nameRef = programBuilder.appendString(name)
    programBuilder.appendGlobalRef(programBuilder.append(ConstantSlot(nameRef)))
    val access = VarAccess(OpGetGlobal(nameRef), OpSetGlobal(nameRef))

    assert(env.isGlobal)
    if(env.lookupVar(name).isDefined)
      throw CompilationException(s"VariableDef: Redefinition of global variable '$name'.")

    env.allocateVar(name, access)
  }

  def compileBlockChildren(methodBuilder: ConstantMethodBuilder, children: Seq[AstNode]): Unit = {
    val iter = children.iterator
    while (iter.hasNext) {
      compileExpr(methodBuilder, iter.next())
      if (iter.hasNext)
        methodBuilder.append(OpDrop())
    }
  }

  def compileMethodDef(name: String, parameters: Seq[String], body: AstNode): ConstantRef[ConstantMethod] = {
    val newMethodBuilder = programBuilder.newMethodBuilder(name, parameters.size)
    env.withFrame {
      parameters.zipWithIndex.collect { case (param, i) =>
        env.allocateVar(param, VarAccess(OpGetLocal(i), OpSetLocal(i)))
      }
      compileExpr(newMethodBuilder, body)
    }
    newMethodBuilder.append(OpReturn())
    programBuilder.append(newMethodBuilder)
  }

  def compileCallMethod(methodBuilder: ConstantMethodBuilder, obj: AstNode, name: String, arguments: Seq[AstNode]): Unit = {
    compileExpr(methodBuilder, obj)
    compileCallMethod(methodBuilder, name, arguments)
  }

  def compileCallMethod(methodBuilder: ConstantMethodBuilder, name: String, arguments: Seq[AstNode]): Unit = {
    arguments.foreach(compileExpr(methodBuilder, _))
    methodBuilder.append(OpCallMethod(programBuilder.appendString(name), arguments.size + 1)) // 1 for this
  }

  def compileArrayDef(methodBuilder: ConstantMethodBuilder, size: AstNode, value: AstNode): Unit = {
    compileExpr(methodBuilder, size)
    val arrayLenLocal = methodBuilder.appendLocal()
    methodBuilder.append(OpSetLocal(arrayLenLocal))
    methodBuilder.append(OpLiteral(programBuilder.appendNull()))
    methodBuilder.append(OpArray())
    val arrayLocal = methodBuilder.appendLocal()
    methodBuilder.append(OpSetLocal(arrayLocal))

    // stack now: array

    // var i = 0;
    val iLocal = methodBuilder.appendLocal()
    methodBuilder.append(OpLiteral(programBuilder.appendInt(0)))
    methodBuilder.append(OpSetLocal(iLocal))
    methodBuilder.append(OpDrop())

    // cond:
    // if i >= arrayLen: goto end
    val condLabel = programBuilder.appendLabelName("arrayCond")
    val endLabel = programBuilder.appendLabelName("arrayEnd")
    methodBuilder.append(OpLabel(condLabel))
    methodBuilder.append(OpGetLocal(iLocal))
    methodBuilder.append(OpGetLocal(arrayLenLocal))
    methodBuilder.append(OpCallMethod(programBuilder.appendString(">="), 2))
    methodBuilder.append(OpBranch(endLabel))

    // array.set(i, value);
    methodBuilder.append(OpGetLocal(iLocal))
    compileExpr(methodBuilder, value)
    methodBuilder.append(OpCallMethod(programBuilder.appendString("set"), 3))
    methodBuilder.append(OpDrop())

    // i += 1
    methodBuilder.append(OpGetLocal(iLocal))
    methodBuilder.append(OpLiteral(programBuilder.appendInt(1)))
    methodBuilder.append(OpCallMethod(programBuilder.appendString("+"), 2))
    methodBuilder.append(OpSetLocal(iLocal))
    methodBuilder.append(OpDrop())

    // goto cond;
    // end:
    methodBuilder.append(OpGetLocal(arrayLocal))
    methodBuilder.append(OpJump(condLabel))
    methodBuilder.append(OpLabel(endLabel))
  }
}

object AstToBcCompiler {
  case class VarAccess(getInsn: SerializedInsn, setInsn: SerializedInsn)

  class CompilationEnv {
    type EnvFrame = Map[String, VarAccess]

    private var _env: EnvFrame = Map.empty
    private var _isGlobal: Boolean = true

    def withFrame[R](thunk: => R): R = {
      val oldEnv = _env
      val oldIsGlobal = _isGlobal
      _isGlobal = false
      try
        thunk
      finally {
        _env = oldEnv
        _isGlobal = oldIsGlobal
      }
    }

    def isGlobal: Boolean = _isGlobal

    def lookupVar(name: String): Option[VarAccess] = _env.get(name)

    def allocateVar(name: String, value: VarAccess): VarAccess = {
      _env += (name -> value)
      value
    }
  }
}
package fml

import java.nio.file.{Files, Path}

import fml.ast.{AstNode, AstParser}
import fml.bc.{Program, ProgramReader}
import fml.compiler.AstToBcCompiler
import fml.interpreter.{AstInterpreter, BcInterpreter}
import fml.state.HeapFactory

import scala.util.{Try, Using}

object Main extends App {

  sealed trait ProgramMode
  sealed trait TakesInputFile extends ProgramMode {
    def inputFile: String
  }
  sealed trait TakesAstInputFile extends TakesInputFile {
    def readInput: Try[AstNode] = Using(Files.newBufferedReader(Path.of(inputFile))) { reader =>
      AstParser.parse(reader)
    }
  }
  sealed trait TakesBytecodeInputFile extends TakesInputFile {
    def readInput: Try[Program] = Using(Files.newInputStream(Path.of(inputFile))) { stream =>
      ProgramReader.read(stream)
    }
  }

  case class Help() extends ProgramMode
  case class DumpAst(inputFile: String) extends ProgramMode with TakesAstInputFile
  case class Interpret(inputFile: String, heapSizeOption: Option[Int] = None, heapLogOption: Option[Path] = None) extends ProgramMode with TakesAstInputFile
  case class Compile(inputFile: String) extends ProgramMode with TakesAstInputFile
  case class Run(inputFile: String, heapSizeOption: Option[Int] = None, heapLogOption: Option[Path] = None) extends ProgramMode with TakesAstInputFile
  case class Execute(inputFile: String, heapSizeOption: Option[Int] = None, heapLogOption: Option[Path] = None) extends ProgramMode with TakesBytecodeInputFile
  case class Server(port: Int) extends ProgramMode

  private def parseOptions(mode: Interpret, args: Seq[String]): Interpret = args match {
    case Seq() => mode
    case "--heap-log" +: arg +: tail => parseOptions(mode.copy(heapLogOption = Some(Path.of(arg))), tail)
    case "--heap-size" +: arg +: tail => parseOptions(mode.copy(heapSizeOption = Some(arg.toInt * 1000000)), tail)

    case arg +: _ => throw new IllegalArgumentException(s"Unknown option: $arg")
  }

  private def parseOptions(mode: Run, args: Seq[String]): Run = args match {
    case Seq() => mode
    case "--heap-log" +: arg +: tail => parseOptions(mode.copy(heapLogOption = Some(Path.of(arg))), tail)
    case "--heap-size" +: arg +: tail => parseOptions(mode.copy(heapSizeOption = Some(arg.toInt * 1000000)), tail)

    case arg +: _ => throw new IllegalArgumentException(s"Unknown option: $arg")
  }

  private def parseOptions(mode: Execute, args: Seq[String]): Execute = args match {
    case Seq() => mode
    case "--heap-log" +: arg +: tail => parseOptions(mode.copy(heapLogOption = Some(Path.of(arg))), tail)
    case "--heap-size" +: arg +: tail => parseOptions(mode.copy(heapSizeOption = Some(arg.toInt * 1000000)), tail)

    case arg +: _ => throw new IllegalArgumentException(s"Unknown option: $arg")
  }

  private def parseArgs(args: Seq[String]): Option[ProgramMode] = args match {
    case Seq() => None
    case ("-h" | "--help") +: _ => Some(Help())
    case "dump" +: arg +: _ => Some(DumpAst(arg))
    case "interpret" +: arg +: tail => Some(parseOptions(Interpret(arg), tail))
    case "execute" +: arg +: tail => Some(parseOptions(Execute(arg), tail))
    case "compile" +: arg +: _ => Some(Compile(arg))
    case "server" +: arg +: _ => Some(Server(arg.toInt))

    case arg +: _ => throw new IllegalArgumentException(s"Unknown option: $arg")
  }

  private def displayHelpAndExit(status: Int): Unit = {
    Console.println(
      """Usage: fml.jar [-h|--help] ACTION
        |
        |ACTION is one of:
        |
        |  dump <astInputFile>
        |  interpret [--heap-size heapSizeMb] [--heap-log heapLogFile] <astInputFile>
        |  run [--heap-size heapSizeMb] [--heap-log heapLogFile] <astInputFile>
        |  execute [--heap-size heapSizeMb] [--heap-log heapLogFile] <bcInputFile>
        |  compile <astInputFile>
        |  server <port>
        |""".stripMargin)
    sys.exit(status)
  }

  private val mode = parseArgs(args.toIndexedSeq)

  mode match {
    case None => displayHelpAndExit(1)
    case Some(Help()) => displayHelpAndExit(0)

    case Some(d: DumpAst) => Console.println(d.readInput.get)
    case Some(i: Interpret) => new AstInterpreter(HeapFactory.createUnmanagedHeap(i.heapSizeOption, i.heapLogOption)).eval(i.readInput.get)
    case Some(c: Compile) => new AstToBcCompiler().compile(c.readInput.get).writeTo(System.out)

    case Some(e: Execute) => new BcInterpreter(e.readInput.get, HeapFactory.createGcHeap(e.heapSizeOption, e.heapLogOption)).run()

    case Some(r: Run) =>
      val compiledBuf = new AstToBcCompiler().compile(r.readInput.get).toByteBuffer
      new BcInterpreter(ProgramReader.read(compiledBuf), HeapFactory.createGcHeap(r.heapSizeOption, r.heapLogOption)).run()

    case Some(s: Server) => new FMLServer(s.port).start()
  }
}
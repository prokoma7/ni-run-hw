package fml

import java.io.{BufferedWriter, InputStreamReader, OutputStreamWriter}
import java.net.InetSocketAddress
import java.nio.file.Path
import java.time.LocalTime

import com.sun.net.httpserver.{HttpExchange, HttpServer}
import fml.ast.AstParser
import fml.bc.ProgramReader
import fml.compiler.AstToBcCompiler
import fml.interpreter.BcInterpreter
import fml.state.HeapFactory

import scala.util.Using

class FMLServer(port: Int) {
  private val inst = HttpServer.create(new InetSocketAddress(port), 0)
  inst.createContext("/run", handleRunRequest)
  inst.createContext("/execute", handleExecuteRequest)
  inst.setExecutor(null) // creates a default executor

  def start(): Unit = {
    inst.start()
    Console.err.println(s"FML server listening on *:$port")
  }

  private def handleRunRequest(req: HttpExchange): Unit = {
    Console.println(s"Incoming run request at ${LocalTime.now()}")

    req.sendResponseHeaders(200, 0)

    try {
      val heapSizeOption = Option(req.getRequestHeaders.getFirst("X-Heap-Size-Mb")).map(_.toInt * 1000000)
      val heapLogOption = Option(req.getRequestHeaders.getFirst("X-Heap-Log")).map(Path.of(_))
      val noGc = Option(req.getRequestHeaders.getFirst("X-NoGc")).contains("1")

      val heap = if(noGc) HeapFactory.createNoGcManagedHeap(heapSizeOption, heapLogOption) else HeapFactory.createGcHeap(heapSizeOption, heapLogOption)

      val parsedAst = Using(new InputStreamReader(req.getRequestBody)) { reader =>
        AstParser.parse(reader)
      }.get

      val compiledBuf = new AstToBcCompiler().compile(parsedAst).toByteBuffer

      val os = req.getResponseBody
      Using(new BufferedWriter(new OutputStreamWriter(os))) { writer => {
        new BcInterpreter(ProgramReader.read(compiledBuf), heap) {
          override protected def printStr(str: String): Unit =
            writer.write(str)
        }.run()
      }
      }.get
      os.close()
    } catch {
      case ex: Exception => ex.printStackTrace()
    }
  }

  private def handleExecuteRequest(req: HttpExchange): Unit = {
    Console.println(s"Incoming execute request at ${LocalTime.now()}")

    req.sendResponseHeaders(200, 0)

    try {
      val heapSizeOption = Option(req.getRequestHeaders.getFirst("X-Heap-Size")).map(_.toInt)
      val heapLogOption = Option(req.getRequestHeaders.getFirst("X-Heap-Log")).map(Path.of(_))
      val noGc = Option(req.getRequestHeaders.getFirst("X-NoGc")).contains("1")

      val program = ProgramReader.read(req.getRequestBody)

      val heap = if(noGc) HeapFactory.createNoGcManagedHeap(heapSizeOption, heapLogOption) else HeapFactory.createGcHeap(heapSizeOption, heapLogOption)

      val os = req.getResponseBody
      Using(new BufferedWriter(new OutputStreamWriter(os))) { writer => {
        new BcInterpreter(program, heap) {
          override protected def printStr(str: String): Unit =
            writer.write(str)
        }.run()
      }
      }.get
      os.close()

      System.gc()
    } catch {
      case ex: Exception => ex.printStackTrace()
    }
  }
}

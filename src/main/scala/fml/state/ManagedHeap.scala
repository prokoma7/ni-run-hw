package fml.state

import java.time.Instant

trait GcRoot {
  def ref: HeapElementRef

  def replace(newRef: HeapElementRef): Unit
}

class ManagedHeap(capacity: Int, loggerOption: Option[HeapEvent => Unit], var gcRootSupplier: Option[() => Iterator[GcRoot]] = None) extends Heap(capacity, loggerOption) {
  protected def gcRootIterator: Iterator[GcRoot] = gcRootSupplier.get()

  def gc(): Unit = {}
}

trait CopyingGarbageCollector extends ManagedHeap {
  // first area is <1..midpoint), second area is <midpoint, capacity)

  private implicit def heap: Heap = this

  var liveIsSecond: Boolean = false
  var gcInProgress: Boolean = false
  var gcCtr: Int = 0

  private val midpoint = capacity / 2

  @inline
  protected def liveAreaStart: Int = if(liveIsSecond) midpoint else 1

  @inline
  protected def liveAreaEnd: Int = if(liveIsSecond) (capacity & 0xfffffffe) - 1 else midpoint

  @inline
  def canAllocateElement(totalLength: Int): Boolean =
    buf.position() + totalLength < liveAreaEnd

  override def allocateElement(typeId: Byte, dataLength: Int): HeapElementRef = {
    val totalLength = 1 + dataLength // typeId
    if(!gcInProgress && !canAllocateElement(totalLength))
      gc()
    if(!canAllocateElement(totalLength))
      throw new RuntimeException(s"Out of memory when allocating ${HeapElement(typeId)} with total length $totalLength.")
    super.allocateElement(typeId, dataLength)
  }

  override def usedBytes: Int = buf.position() - liveAreaStart

  private def copyManagedObject(ptr: HeapElementRef): HeapElementRef = ptr match {
    case HeapNull() => allocateNull()
    case HeapInt(value) => allocateInt(value)
    case HeapBoolean(value) => allocateBoolean(value)
    case HeapObject(classId, parentRef, numFields) => allocateObject(classId, parentRef, 0.until(numFields).map(tryGetObjectField(ptr, _)).map(_.get))
    case HeapArray(size) => allocateArray(0.until(size).map(tryGetArrayElement(ptr, _)).map(_.get))
  }

  private def copyManagedObjectDependencies(ptr: HeapElementRef): Unit = ptr match {
    case HeapObject(_, _, numFields) =>
      0.until(numFields).foreach(fieldIndex =>
        putObjectField(ptr, fieldIndex, copyObjectAndDependencies(getObjectField(ptr, fieldIndex))))
    case HeapArray(size) =>
      0.until(size).foreach(i =>
        putArrayElement(ptr, i, copyObjectAndDependencies(getArrayElement(ptr, i))))
    case _ =>
  }

  private def copyObjectAndDependencies(ptr: HeapElementRef): HeapElementRef = ptr match {
    case _ if !isManaged(ptr) => ptr
    case HeapForwardingPtr(target) => target

    case _ =>
      val newPtr = copyManagedObject(ptr)
      putForwardingPtrTarget(ptr, newPtr)
      copyManagedObjectDependencies(newPtr)
      newPtr
  }

  /** Properly handle gc during allocateObject and allocateArray. */

  override def allocateObject(classId: ClassId, parentRef: HeapElementRef, fieldValueRefs: Seq[HeapElementRef]): HeapElementRef = {
    val oldGcCtr = gcCtr
    val objRef = super.allocateObject(classId, parentRef, fieldValueRefs)
    if(oldGcCtr != gcCtr) {
      putObjectParent(objRef, copyObjectAndDependencies(parentRef))
      fieldValueRefs.zipWithIndex.foreach({ case (value, idx) =>
        putObjectField(objRef, idx, copyObjectAndDependencies(value)) })
    }
    objRef
  }

  override def allocateArray(valueRefs: Seq[HeapElementRef]): HeapElementRef = {
    val oldGcCtr = gcCtr
    val arrRef = super.allocateArray(valueRefs)
    if (oldGcCtr != gcCtr)
      valueRefs.zipWithIndex.foreach({ case (value, idx) =>
        putArrayElement(arrRef, idx, copyObjectAndDependencies(value)) })
    arrRef
  }

  override def gc(): Unit = {
    liveIsSecond = !liveIsSecond
    buf.position(liveAreaStart)

    val timeBegin = Instant.now()
    gcInProgress = true
    gcRootIterator.foreach(root => {
//      Console.println(root)
      root.replace(copyObjectAndDependencies(root.ref))
    })
    log(HeapEvent(timeBegin, "G", usedBytes))
    gcInProgress = false
    gcCtr += 1
  }
}

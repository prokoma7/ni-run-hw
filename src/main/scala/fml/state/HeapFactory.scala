package fml.state

import java.nio.file.{Files, Path}

object HeapFactory {
  val defaultGcHeapSize: Int = 10_000_000
  val defaultNoGcHeapSize: Int = 150_000_000

  def createHeapLogger(heapLog: Path): HeapEvent => Unit = {
    val writer = Files.newBufferedWriter(heapLog)
    writer.write("timestamp,event,heap\n")
    event => writer.write(event.toCsv + "\n")
  }

  def createGcHeap(heapSizeOption: Option[Int], heapLogOption: Option[Path]): ManagedHeap =
    new ManagedHeap(heapSizeOption.getOrElse(defaultGcHeapSize), heapLogOption.map(createHeapLogger)) with CopyingGarbageCollector with PointerTagging

  def createNoGcManagedHeap(heapSizeOption: Option[Int], heapLogOption: Option[Path]): ManagedHeap =
    new ManagedHeap(heapSizeOption.getOrElse(defaultNoGcHeapSize), heapLogOption.map(createHeapLogger)) with PointerTagging

  def createUnmanagedHeap(heapSizeOption: Option[Int], heapLogOption: Option[Path]): Heap =
    new Heap(heapSizeOption.getOrElse(defaultNoGcHeapSize), heapLogOption.map(createHeapLogger)) with PointerTagging
}
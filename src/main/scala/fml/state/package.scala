package fml

package object state {
  type HeapElementRef = Long
  type ClassId = Int
}

package fml.state

class ExecutionEnv {
    case class HeapElementRefPtr(var ref: HeapElementRef)
    type EnvFrame = Map[String, HeapElementRefPtr]

    /** The current execution environment frame. */
    private var env: EnvFrame = Map.empty

    /** The top level environment frame. */
    private var globalEnv: Option[EnvFrame] = None

    def withFrame[R](cb: () => R, extendGlobalScope: Boolean = false): R = {
        val oldGlobalEnv = globalEnv
        if(globalEnv.isEmpty)
            globalEnv = Some(env)
        val oldEnv = env
        if(extendGlobalScope)
            env = globalEnv.get
        try
            cb()
        finally {
            globalEnv = oldGlobalEnv
            env = oldEnv
        }
    }

    def lookupVar(name: String): Option[HeapElementRef] = env.get(name).map(_.ref)

    def updateVar(name: String, value: HeapElementRef): Unit = env(name).ref = value

    def allocateVar(name: String, value: HeapElementRef): Unit =
        env += (name -> HeapElementRefPtr(value))
}

package fml.state

import java.nio.ByteBuffer
import java.time.{Instant, LocalDateTime}

sealed trait HeapElement {
  def typeId: Byte
}
object HeapElement {
  def apply(typeId: Int): HeapElement = typeId match {
    case HeapNull.typeId => HeapNull
    case HeapInt.typeId => HeapInt
    case HeapBoolean.typeId => HeapBoolean
    case HeapArray.typeId => HeapArray
    case HeapObject.typeId => HeapObject
  }
}

object HeapNull extends HeapElement {
  val typeId = 0
  val nullPtr = 0L
  def unapply(ptr: HeapElementRef)(implicit heap: Heap): Boolean = heap.tryGetNull(ptr)
}
object HeapInt extends HeapElement {
  val typeId = 1
  def unapply(ptr: HeapElementRef)(implicit heap: Heap): Option[Int] = heap.tryGetInt(ptr)
}
object HeapBoolean extends HeapElement {
  val typeId = 2
  def unapply(ptr: HeapElementRef)(implicit heap: Heap): Option[Boolean] = heap.tryGetBoolean(ptr)
}
object HeapArray extends HeapElement{
  val typeId = 3
  def unapply(ptr: HeapElementRef)(implicit heap: Heap): Option[Int] = heap.tryGetArraySize(ptr)
}
object HeapObject extends HeapElement {
  val typeId = 4
  def unapply(ptr: HeapElementRef)(implicit heap: Heap): Option[(Int, HeapElementRef, Int)] = heap.tryGetObjectInfo(ptr)
}
object HeapForwardingPtr extends HeapElement {
  val typeId = 5
  def unapply(ptr: HeapElementRef)(implicit heap: Heap): Option[HeapElementRef] = heap.tryGetForwardingPtrTarget(ptr)
}

case class HeapEvent(timestamp: Instant, kind: String, data: Int) {
  def toCsv: String = (timestamp.getEpochSecond * 1e9.toLong + timestamp.getNano).toString + "," + kind + "," + data.toString
}

class Heap(val capacity: Int, protected val loggerOption: Option[HeapEvent => Unit] = None) {
  protected val buf: ByteBuffer = ByteBuffer.allocate(capacity)
  buf.put(0.toByte)
  log(HeapEvent(Instant.now(), "S", capacity))

  @inline
  protected def log(thunk: => HeapEvent): Unit = loggerOption.foreach(_(thunk))

  @inline
  private def getArrayElementPtr(ptr: HeapElementRef, index: Int): HeapElementRef =
    ptr + 1 + 4 + 4 + 8 * index // typeId, size

  @inline
  private def getObjectFieldPtr(ptr: HeapElementRef, index: Int): HeapElementRef =
    ptr + 1 + 4 + 8 + 4 + 8 * index // typeId, objectId, parentRef, size

  def getTypeId(ptr: HeapElementRef): Byte = ptr match {
    case HeapNull.nullPtr => HeapNull.typeId
    case _ => buf.get(ptr.toInt)
  }

  def tryGetNull(ptr: HeapElementRef): Boolean = getTypeId(ptr) match {
    case HeapNull.nullPtr => true
    case _ => false
  }

  def tryGetInt(ptr: HeapElementRef): Option[Int] = getTypeId(ptr) match {
    case HeapInt.typeId => Some(buf.getInt((ptr + 1).toInt))
    case _ => None
  }

  def tryGetBoolean(ptr: HeapElementRef): Option[Boolean] = getTypeId(ptr) match {
    case HeapBoolean.typeId => Some(buf.get((ptr + 1).toInt) == 1)
    case _ => None
  }

  def tryGetArraySize(ptr: HeapElementRef): Option[Int] = getTypeId(ptr) match {
    case HeapArray.typeId => Some(buf.getInt((ptr + 1).toInt))
    case _ => None
  }

  def getArrayElement(ptr: HeapElementRef, index: Int): HeapElementRef = buf.getLong(getArrayElementPtr(ptr, index).toInt)

  def tryGetArrayElement(ptr: HeapElementRef, index: Int): Option[HeapElementRef] = getTypeId(ptr) match {
    case HeapArray.typeId => Some(buf.getLong(getArrayElementPtr(ptr, index).toInt))
    case _ => None
  }

  def tryGetObjectInfo(ptr: HeapElementRef): Option[(Int, HeapElementRef, Int)] = getTypeId(ptr) match {
    case HeapObject.typeId => Some((buf.getInt((ptr + 1).toInt), buf.getLong((ptr + 1 + 4).toInt), buf.getInt((ptr + 1 + 4 + 8).toInt)))
    case _ => None
  }

  def getObjectField(ptr: HeapElementRef, index: Int): HeapElementRef = buf.getLong(getObjectFieldPtr(ptr, index).toInt)

  def tryGetObjectField(ptr: HeapElementRef, index: Int): Option[HeapElementRef] = getTypeId(ptr) match {
    case HeapObject.typeId => Some(buf.getLong(getObjectFieldPtr(ptr, index).toInt))
    case _ => None
  }

  def tryGetForwardingPtrTarget(ptr: HeapElementRef): Option[HeapElementRef] = getTypeId(ptr) match {
    case HeapForwardingPtr.typeId => Some(buf.getLong((ptr + 1).toInt))
    case _ => None
  }

  def allocateElement(typeId: Byte, dataLength: Int): HeapElementRef = {
    log(HeapEvent(Instant.now(), "A", usedBytes + 1 + dataLength))
    val ptr = buf.position().toLong
    buf.put(typeId)
    ptr
  }

  def allocateInt(value: Int): HeapElementRef = {
    val ptr = allocateElement(HeapInt.typeId, 4)
    buf.putInt(value)
    buf.position(buf.position() + 4)
    ptr
  }

  def allocateBoolean(value: Boolean): HeapElementRef = {
    val ptr = allocateElement(HeapBoolean.typeId, 1)
    buf.put(if(value) 1.toByte else 0.toByte)
    buf.position(buf.position() + 7)
    ptr
  }

  @inline
  def allocateNull(): HeapElementRef = HeapNull.nullPtr

  def allocateArray(valueRefs: Seq[HeapElementRef]): HeapElementRef = {
    val ptr = allocateElement(HeapArray.typeId, 4 + 8 * valueRefs.size)
    buf.putInt(valueRefs.size)
    buf.position(buf.position() + 4)
    valueRefs.foreach(ref => buf.putLong(ref))
    ptr
  }

  def allocateObject(classId: Int, parentRef: HeapElementRef, fieldValueRefs: Seq[HeapElementRef]): HeapElementRef = {
    val ptr = allocateElement(HeapObject.typeId, 4 + 8 + 4 + 8 * fieldValueRefs.size)
    buf.putInt(classId)
    buf.putLong(parentRef)
    buf.putInt(fieldValueRefs.size)
    fieldValueRefs.foreach(ref => buf.putLong(ref))
    ptr
  }

  def putArrayElement(ptr: HeapElementRef, index: Int, valueRef: HeapElementRef): Unit = {
    buf.putLong(getArrayElementPtr(ptr, index).toInt, valueRef)
  }

  def putObjectField(ptr: HeapElementRef, index: Int, valueRef: HeapElementRef): Unit = {
    buf.putLong(getObjectFieldPtr(ptr, index).toInt, valueRef)
  }

  def putObjectParent(ptr: HeapElementRef, parentRef: HeapElementRef): Unit = {
    buf.putLong((ptr + 1 + 4).toInt, parentRef)
  }

  def putForwardingPtrTarget(ptr: HeapElementRef, targetRef: HeapElementRef): Unit = {
    buf.put(ptr.toInt, HeapForwardingPtr.typeId)
    buf.putLong((ptr + 1).toInt, targetRef)
  }

  def usedBytes: Int = buf.position()

  /** Returns true if the pointer is in fact stored in the heap. */
  def isManaged(ptr: HeapElementRef): Boolean = ptr != HeapNull.nullPtr
}

trait PointerTagging extends Heap {
  @inline
  def splitPointer1(ptr: HeapElementRef): Byte =
    (ptr >>> 56).toByte

  @inline
  def splitPointer2(ptr: HeapElementRef): Long =
    ptr & 0xffffffffffffffL

  @inline
  def joinPointer(tag: Byte, data: Long): HeapElementRef =
    (data & 0xffffffffffffffL) | (tag.toLong << 56)

  override def getTypeId(ptr: HeapElementRef): Byte = splitPointer1(ptr) match {
    case 0 => super.getTypeId(ptr)
    case typeId => typeId
  }

  override def allocateInt(value: Int): HeapElementRef =
    joinPointer(HeapInt.typeId, value.toLong)

  override def allocateBoolean(value: Boolean): HeapElementRef =
    joinPointer(HeapBoolean.typeId, if(value) 1.toByte else 0.toByte)

  override def tryGetInt(ptr: HeapElementRef): Option[Int] = getTypeId(ptr) match {
    case HeapInt.typeId => Some(splitPointer2(ptr).toInt)
    case _ => None
  }

  override def tryGetBoolean(ptr: HeapElementRef): Option[Boolean] = getTypeId(ptr) match {
    case HeapBoolean.typeId => Some(splitPointer2(ptr) == 1)
    case _ => None
  }

  override def isManaged(ptr: HeapElementRef): Boolean = splitPointer1(ptr) match {
    case HeapInt.typeId | HeapBoolean.typeId => false
    case _ => super.isManaged(ptr)
  }
}